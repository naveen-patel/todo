import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../../app/myservice.service';
import * as moment from 'moment';
@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {
  task: '';
  taskList: any = [];
  i = 0;
  k = 0;
  a; b;
  rowvalue: '';
  now;
  class: any;
  randomcolor: any = ['bg-primary', 'bg-secondary', 'bg-success', 'bg-danger', 'bg-warning', 'bg-info', 'bg-dark'];
  color: any;
  getTasks: any;
  getTask: any;
  retainTasks: any;
  savetask: any = [];
  constructor(private MyserviceService: MyserviceService) {
  }

  addtask() {
    this.now = moment();
    console.log('hello world', this.now.format('DD/MM/YYYY'));
    this.color = this.randomcolor[Math.floor(Math.random() * this.randomcolor.length)];
    this.retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
    // console.log( 'date ', this.now());
    this.b = new Date();
    // this.a = Moment(this.b).format('DD/MM/YYYY') ;
    this.retainTasks.push({ task: this.task, date: this.now.format('DD/MM/YYYY'), color: this.color });
    window.localStorage.setItem('arrayList', JSON.stringify(this.retainTasks));
    this.getTask = JSON.parse(window.localStorage.getItem('arrayList'));
    console.log(this.color);
  }
  edittask() {
    this.retainTasks = JSON.parse(window.localStorage.getItem('arrayList'));
    console.log('array', this.retainTasks);
    this.retainTasks.push({ task: this.task, date: new Date(), color: this.color });
    window.localStorage.setItem('arrayList', JSON.stringify(this.retainTasks));
    this.getTask = JSON.parse(window.localStorage.getItem('arrayList'));
  }
  deletetask() {
    console.log('delete value', this.rowvalue);
  }
  ngOnInit() {
    // for setting array in local storage if not exist
    this.getTasks = window.localStorage.getItem('arrayList');
    if (this.getTasks == null || this.getTasks === undefined || this.getTasks === '') {
      window.localStorage.setItem('arrayList', JSON.stringify([]));
    }
    this.getTask = JSON.parse(window.localStorage.getItem('arrayList'));
  }
}
