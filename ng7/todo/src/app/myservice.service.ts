import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MyserviceService {
    public BASE_URL = 'http://10.0.60.93:3001/';
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // tslint:disable-next-line:max-line-length
        Authorization: 'My token'
      })
    };
    constructor(private httpclient: HttpClient) { }
    // post api
  //   // post(obj: task): Observable<any> {
  //     return this.httpclient.post(this.BASE_URL + 'verify', obj);
  // }
}
