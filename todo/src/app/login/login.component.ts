import { Component, OnInit } from '@angular/core';
import {ServiceService} from '../service.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private apicall: ServiceService) { }

  ngOnInit() {
    this.apicall.getHeroes();
    console.log(this.apicall.apicall().subscribe());
  }

}
