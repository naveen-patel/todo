import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
url = 'https://jsonplaceholder.typicode.com/todos/1';
  constructor(  private http: HttpClient) { }
  getHeroes(): string {
    return 'Hello';
  }
apicall() {
    return this.http.get<Observable<any>>(this.url);
}


}

