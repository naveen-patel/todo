"use strict";
exports.__esModule = true;
function hello(person) {
    //return "Hello, "+ person.firstName + " "+ person.lastName;
    return "Hello, " + person.firstName + " " + person.lastName;
}
var person = { firstName: 'Naksh', lastName: 'Patel' };
console.log(hello(person));

