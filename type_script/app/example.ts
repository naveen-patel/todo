// // function ram(this:{a: number, b: number}, a: number, b: number)  {
// //     this.a = a;
// //     this.b = b;
// //     return this;
// //   }

// //   let z = ram(1,2);
// //   console.log(z);

// //   function add (a: number, b: number): number {
// //     return a + b
// //   }
// //   add(2,3);


// interface myThis{
//   a: number,
//   b: number
// }

// function object(a: number, b: number):myThis {
//   this.a = a;
//   this.b = b;
//   return this;
// }

// // this is used only for type declaration
// let a:myThis = object.call(this, 1,2);
// console.log(a);
// // a has type {a: number, b: number}