import {Person } from './person';

function hello(person:Person):string{

    //return "Hello, "+ person.firstName + " "+ person.lastName;
    return `Hello, ${person.firstName} ${person.lastName}`;
}
let person:Person={firstName:'Naksh', lastName:'Patel'};
console.log(hello(person));